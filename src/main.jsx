import './main.scss';

const currentMilesInput = document.body.querySelector('#current-miles');
const totalMilesOutput = document.body.querySelector('#estimated-total-miles');
const leaseStartInput = document.body.querySelector('#lease-start-date');
const leaseEndInput = document.body.querySelector('#lease-end-date');
const daysDrivenOutput = document.body.querySelector('#days-driven');
const milesPerDayOutput = document.body.querySelector('#miles-per-day');
const leaseCompletionOutput = document.body.querySelector('#lease-completion');

const myStartDate = new Date('6/27/2020');
const myEndDate = new Date('6/27/2023');
leaseStartInput.value = myStartDate.toJSON().split('T')[0];
leaseEndInput.value = myEndDate.toJSON().split('T')[0];

const data = {
  milesDriven: 0,
  leaseStartDate: myStartDate,
  leaseEndDate: myEndDate,
};

const calculate = () => {
  const today = new Date();
  const daysDriven = (today - data.leaseStartDate) / (1000 * 60 * 60 * 24);
  const totalDays = (data.leaseEndDate.getTime() - data.leaseStartDate.getTime()) / (1000 * 3600 * 24);
  const milesPerDay = data.milesDriven / daysDriven;
  const projectedMiles = milesPerDay * totalDays;
  leaseCompletionOutput.value = `${Math.round(100 * (daysDriven / totalDays))}%`;
  milesPerDayOutput.value = Math.round(milesPerDay);
  daysDrivenOutput.value = Math.round(daysDriven);
  totalMilesOutput.value = Math.round(projectedMiles);
};


leaseStartInput.addEventListener('change', () => {
  const parts = leaseStartInput.value.split('-');
  data.leaseStartDate = new Date(`${parts[1]}/${parts[2]}/${parts[0]}`);
  calculate();
});
leaseEndInput.addEventListener('change', () => {
  const parts = leaseEndInput.value.split('-');
  data.leaseEndDate = new Date(`${parts[1]}/${parts[2]}/${parts[0]}`);
  calculate();
});
currentMilesInput.addEventListener('input', () => {
  data.milesDriven = parseInt(currentMilesInput.value, 10);
  calculate();
});

calculate();
