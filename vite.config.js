import { defineConfig } from 'vite';

export default defineConfig({
  base: '/mileage-calc/',
  server: {
    port: 8080,
    host: true,
  },
});
